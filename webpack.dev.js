const path = require('path');
const config = require('./webpack.config');

module.exports = {
  mode: 'development',
  output: {
    clean: true,
    path: path.resolve(__dirname, 'build'),
    filename: '[fullhash].js',
  },
  devServer: {
    static: {
      directory: path.resolve(__dirname, 'src/public'),
    },
    port: 3000,
    open: true,
    hot: true,
    historyApiFallback: true,
  },
  ...config,
};
