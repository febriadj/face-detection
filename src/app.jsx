import React, { useEffect } from 'react';
import * as faceapi from 'face-api.js';
import './style.css';

function App() {
  const modelUri = '/models';

  const handleMediaPlay = async (video) => {
    try {
      const canvas = await faceapi.createCanvasFromMedia(video);
      document.body.append(canvas);

      const detections = await faceapi.detectAllFaces(
        video,
        new faceapi.TinyFaceDetectorOptions(),
      )
        .withFaceLandmarks()
        .withFaceExpressions();

      const displaySize = { width: video.width, height: video.height };
      const resizedDetections = faceapi.resizeResults(detections, displaySize);

      faceapi.draw.drawFaceExpressions(canvas, resizedDetections);
    }
    catch (error0) {
      console.error(error0.message);
    }
  };

  const handleSetMedia = async () => {
    try {
      await faceapi.nets.tinyFaceDetector.loadFromUri(modelUri);
      await faceapi.nets.ssdMobilenetv1.loadFromUri(modelUri);
      await faceapi.nets.faceRecognitionNet.loadFromUri(modelUri);
      await faceapi.nets.faceLandmark68Net.loadFromUri(modelUri);
      await faceapi.nets.faceExpressionNet.loadFromUri(modelUri);

      const video = document.querySelector('video');
      video.srcObject = await navigator.mediaDevices.getUserMedia({
        video: true,
      });

      video.onplay = () => {
        handleMediaPlay(video);
      };
    }
    catch (error0) {
      console.error(error0);
    }
  };

  useEffect(() => {
    document.title = 'Febriadji - Face Detection';
    handleSetMedia();
  }, []);

  return (
    <div className="absolute w-full h-full flex justify-center items-center bg-gray-100">
      <div className="relative w-96 m-5">
        <video
          autoPlay
          muted
          className="absolute w-full h-full"
        >
        </video>
        <canvas className="absolute w-full h-full"></canvas>
      </div>
    </div>
  );
}

export default App;
